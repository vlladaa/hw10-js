const tabs = document.querySelector(".tabs")
const tabsContent = document.querySelectorAll('.tabs-content li')

tabs.addEventListener('click', (event) => {
    const btn = event.target;
    const id = btn.dataset.text //#text_garen
    const idActive = document.querySelector(id) //выбор элемента по айди text_garen
    
    tabs.querySelectorAll('.active').forEach(element => {
        element.classList.remove('active')
    }); //удалить класс active для title табов

    tabsContent.forEach(element => {
        element.classList.remove('active')
    }); 

    btn.classList.add('active');
    idActive.classList.add('active')
    }
    
)


